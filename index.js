const users = require("./users");
const items = require('./items');
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors')

const app = express();
const PORT = process.env.PORT || 3000;

app.use(cors())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true}));

app.listen(PORT, function(err, data){
   if(err){
       console.log(err);
   }
   else{
       console.log("connected");
   }
});

app.post("/users", function(req, res){
    users.saveData(req.body, function(err, data){
        res.send(data);
    });
});


app.get("/users", function (req, res){
    users.getData(function (data) {
        let fireBaseData = Object.entries(data);
        let users = [];

        fireBaseData.forEach(function(user) {
            users.push(user[1]);
        });
        res.send(users);
    });
});

app.post("/items", function(req, res){
    items.saveData(req.body, function(err, data){
        res.send(data);
    });
});

app.put("/users", function (req, res) {
    users.updateData(req.body, function(data){
        res.send({"status": "200", "message": "updated"});
    })
});

app.get("/items", function(req, res){
    items.getData(function(data){
        let fireBaseData = Object.entries(data);
        let items = [];

        fireBaseData.forEach(function(item) {
            items.push(item[1]);
        });
        res.send(items);
    });
});


app.put("/items", function (req, res) {
    items.updateData(req.body, function(data){
        res.send({"status": "200", "message": "updated"});
    })
});

app.delete("/items/:title", function (req, res) {
    items.deleteData(function(data){
        res.send({"status": 200, "message": "deleted"});
    });
});
