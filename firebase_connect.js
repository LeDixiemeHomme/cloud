require('dotenv').config();
const firebase = require('firebase');

const apiKey = process.env.apiKey || "AIzaSyDdsMrWj1wcvv4vWrbrCrUixYClL1f6b84";
const authDomain = process.env.authDomain || "todolist-prod-4dac6.firebaseapp.com";
const databaseURL = process.env.databaseURL || "https://todolist-prod-4dac6-default-rtdb.firebaseio.com";
const projectId = process.env.projectId || "todolist-prod-4dac6";
const storageBucket = process.env.storageBucket || "todolist-prod-4dac6.appspot.com";
const messagingSenderId = process.env.messagingSenderId || "979356624043";
const appId = process.env.appId || "1:979356624043:web:fd528226f947349282c3ee";


const app = firebase.initializeApp({
    apiKey: apiKey,
    authDomain: authDomain,
    databaseURL: databaseURL,
    projectId: projectId,
    storageBucket: storageBucket,
    messagingSenderId: messagingSenderId,
    appId: appId,
});


module.exports = app;
