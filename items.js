const firebase = require('./firebase_connect');

module.exports = {
    saveData: function (req, callback) {
        let title = req.title;

        firebase.database().ref("items/" + title).set({
            title: req.title,
            user: req.user,
            status: req.status,
            content: req.content,
            position: req.position,
        });
        callback(null, {"statuscode": 201, "message": "Item added"});
    },

    getData: function(callback) {
        firebase.database().ref("items/").once("value").then(function(snapshot){
            callback(snapshot.val());
        });
    },

    updateData: function(req, callback) {
        let title = req.title;
        let user = req.user;
        let status = req.status;
        let content = req.content;
        let position = req.position;

        firebase.database().ref("items/" + title + "/").update({
            title: title,
            user: user,
            status: status,
            content: content,
            position: position,
        });
        callback("item modified");
    },
};
