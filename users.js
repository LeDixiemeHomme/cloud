const firebase = require('./firebase_connect');

module.exports = {
    saveData: function (req, callback) {
        let username = req.username;

        firebase.database().ref("users/" + username).set({
            name: req.username,
            email: req.email,
        });
        callback(null, {"statuscode":201, "message": "User inserted"});
    },

    getData: function(callback){
        firebase.database().ref("users/").once("value").then(function(snapshot){
            callback(snapshot.val());
        });
    },

    updateData: function(req, callback) {
        let name =  req.username;
        let email = req.email;

        firebase.database().ref("users/" + username + "/").update({
            name: name,
            email: email
        });
        callback("user modified");
    },
};
